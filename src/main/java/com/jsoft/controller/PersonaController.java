package com.jsoft.controller;

import java.util.Map;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.jsoft.model.Persona;
import com.jsoft.service.PersonaService;



@Controller
public class PersonaController {
	
	
	private PersonaService personaService;
	
	public PersonaController(PersonaService personaService) {
		this.personaService = personaService;
	}
	
	@GetMapping("/personas")
	public String persona(Map<String, Object> model){
		model.put("personas", personaService.listAll());
		return "personas";
	}
	@PostMapping("/personas")
	public String save(Persona persona){
		personaService.savePersona(persona);
		return "redirect:/personas";
		
	}


}
