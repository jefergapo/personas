package com.jsoft.repository;

import org.springframework.data.repository.CrudRepository;

import com.jsoft.model.Persona;



public interface PersonaRepository extends CrudRepository<Persona, Long>{

}
