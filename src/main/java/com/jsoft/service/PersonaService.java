package com.jsoft.service;

import com.jsoft.model.Persona;

public interface PersonaService {
	
	Iterable<Persona> listAll();
	
	Persona getPersonaById(Long id);
	
	Persona savePersona(Persona persona);
	
	void deletePersona(Long id);

}
