package com.jsoft.service;

import org.springframework.stereotype.Service;

import com.jsoft.model.Persona;
import com.jsoft.repository.PersonaRepository;

@Service
public class PersonaServiceImpl implements PersonaService{
	
	private PersonaRepository personaRepository;
	
	
	
	public PersonaServiceImpl(PersonaRepository personaRepository) {
		this.personaRepository = personaRepository;
	}


	public Iterable<Persona> listAll() {
		return personaRepository.findAll();
	}


	@Override
	public Persona getPersonaById(Long id) {
		
		return personaRepository.findOne(id);
	}


	@Override
	public Persona savePersona(Persona persona) {
		return personaRepository.save(persona);
	}


	@Override
	public void deletePersona(Long id) {
		personaRepository.delete(id);
		
	}

}
